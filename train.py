import argparse
import os

from torch.utils.data import DataLoader
import torch.optim
import math
import time
import torchvision.transforms as transforms
from models.utils import load_checkpoint, save_checkpoint, print_cur_info, draw_box_on_imgs
from torch import nn
from models.model import MODEL
from data import MovingCoil
from torch.nn.utils import clip_grad_norm_
from models.common import *
from torchvision.utils import make_grid

from tensorboardX import SummaryWriter

def main():

    parser = argparse.ArgumentParser(description='Train!')
    parser.add_argument('--training-data-dir', default='./coil_data/training', metavar='DIR',
                        help='diretory of the training data')
    parser.add_argument('--lr', type=float, default=5e-5,
                        help="learning rate")
    parser.add_argument('-b', '--batch-size', type=int, default=8,
                        help="batch size")
    parser.add_argument('-epochs', type=int, default=50000,
                        help="epochs size")
    parser.add_argument('-p', '--show-frequency', default=50,
                        help="the frequency of printing the value of loss")
    parser.add_argument('--save-checkpoint-frequency', type=int, default=1,
                        help='the frequency (epoch) to save checkpoint')
    parser.add_argument('--checkpoint-save-path', type=str, default='model',
                        help='the path to save trained parameters')
    parser.add_argument('--checkpoint-load-path', type=str, default='model',
                        help='the path to load the trained parameters')
    parser.add_argument('--checkpoint_file', type=str, default='',
                        help='file name of the parameters to be loaded')
    parser.add_argument('--end-epochs', type=int, default=60,
                        help='end epochs')
    parser.add_argument('--sigma', default=0.1, type=float, metavar='S',
                        help='Sigma for log likelihood.')
    parser.add_argument('--summary-path', type=str, default='summary',
                        help='the path to save the summary file')
    parser.add_argument('--tau-base', default=0.98, type=float,
                        help='base for the decay of tau')
    parser.add_argument('--max-gradient', default=1.0, type=float,
                        metavar='CP', help='rate of gradient clipping')

    args = parser.parse_args()

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    model = MODEL(sigma=args.sigma)
    model.to(device)
    model.train()

    optimizer = torch.optim.RMSprop(model.parameters(), lr=args.lr)

    if not os.path.exists(args.checkpoint_save_path):
        os.mkdir(args.checkpoint_save_path)

    if not os.path.exists(args.summary_path):
        os.mkdir(args.summary_path)

    if args.checkpoint_file:
        ckpt_file = os.path.join(args.checkpoint_load_path, args.checkpoint_file)
        start_epoch, global_step = load_checkpoint(model, ckpt_file, optimizer)
    else:
        global_step = 0
        start_epoch = 0

    trainset = MovingCoil(root=args.training_data_dir)

    trainloader = DataLoader(trainset, batch_size=args.batch_size,
                             shuffle=True)

    writer = SummaryWriter(args.summary_path)

    start_time = 0
    for epoch in range(start_epoch, args.end_epochs):
        tau = args.tau_base ** epoch

        for batch_idx, sample in enumerate(trainloader):

            global_step += 1

            show = global_step % args.show_frequency == 0 or global_step == 1

            imgs = sample.to(device)

            log_like, kl_z_what, kl_z_where, kl_z_depth, \
            kl_z_pres, detection_info, tracking_info_list, info_list = \
                model(imgs, tau, show=show)

            log_like = log_like.mean(dim=0)
            kl_z_what = kl_z_what.mean(dim=0)
            kl_z_where = kl_z_where.mean(dim=0)
            kl_z_depth = kl_z_depth.mean(dim=0)
            kl_z_pres = kl_z_pres.mean(dim=0)

            loss = - (log_like - kl_z_what - kl_z_where - kl_z_depth - kl_z_pres)

            optimizer.zero_grad()
            loss.backward()

            clip_grad_norm_(model.parameters(), args.max_gradient)
            optimizer.step()

            if show:
                bs = args.batch_size
                end_time = time.time()
                print_cur_info(global_step, epoch, batch_idx, len(trainloader), loss, log_like, kl_z_what, kl_z_where,
                kl_z_pres, kl_z_depth, end_time - start_time)

                start_time = end_time

                writer.add_scalar('train/total_loss', loss.item(), global_step=global_step)
                writer.add_scalar('train/log_like', log_like.item(), global_step=global_step)
                writer.add_scalar('train/What_KL', kl_z_what.item(), global_step=global_step)
                writer.add_scalar('train/Where_KL', kl_z_where.item(), global_step=global_step)
                writer.add_scalar('train/Pres_KL', kl_z_pres.item(), global_step=global_step)
                writer.add_scalar('train/Depth_KL', kl_z_depth.item(), global_step=global_step)
                writer.add_scalar('train/tau', tau, global_step=global_step)

                for name, param in model.named_parameters():
                    writer.add_histogram(name, param.cpu().detach().numpy(), global_step)
                    if param.grad is not None:
                        writer.add_histogram('grad/' + name, param.grad.cpu().detach(), global_step)

                grid_image = make_grid(sample.view(-1, img_channel, img_w, img_h), seq_len, normalize=True, pad_value=1)
                writer.add_image(f'sample/sample', grid_image, global_step)

                detect_recon = detection_info['y'].cpu().detach()
                detect_atten_mask = detection_info['atten_mask'].cpu().detach()
                z_pres = detection_info['z_pres'].cpu().detach()
                z_where = detection_info['z_where'].cpu().detach()
                detect_img_with_box = draw_box_on_imgs(sample[:, 0].unsqueeze(1).expand(bs, 16, img_channel, img_w, img_h),
                                                       z_pres, z_where, apart=True)

                for i in range(bs):

                    grid_image = make_grid(detect_img_with_box[i], 4, normalize=True, pad_value=1)
                    writer.add_image(f'detect/1-imgs_with_box_{i}', grid_image, global_step)

                    grid_image = make_grid(detect_recon[i], 4, normalize=True, pad_value=1)
                    writer.add_image(f'detect/2-recon_imgs_{i}', grid_image, global_step)

                    grid_image = make_grid(detect_atten_mask[i], 4, normalize=True, pad_value=1)
                    writer.add_image(f'detect/2-atten_mask_{i}', grid_image, global_step)

                tracking_recon_imgs = torch.zeros(bs, seq_len, img_channel, img_h, img_w)
                tracking_imgs_with_box = torch.zeros(bs, seq_len, img_channel, img_h, img_w)

                if tracking_info_list:
                    for i in range(seq_len):
                        recon_img = tracking_info_list[i]['y'].cpu().detach()
                        tracking_recon_imgs[:, i] = recon_img

                        z_where = tracking_info_list[i]['z_where'].cpu().detach()
                        z_pres = torch.ones(bs, z_where.size(1), z_pres_dim)

                        img_with_box = draw_box_on_imgs(sample[:, i], z_pres, z_where)

                        tracking_imgs_with_box[:, i] = img_with_box

                    grid_image = make_grid(tracking_imgs_with_box.view(-1, img_channel, img_h, img_w),
                                           seq_len, normalize=True, pad_value=1)
                    writer.add_image(f'track/1-imgs_with_box_{i}', grid_image, global_step)

                    grid_image = make_grid(tracking_recon_imgs.view(-1, img_channel, img_h, img_w),
                                           seq_len, normalize=True, pad_value=1)
                    writer.add_image(f'track/2-recon_imgs_{i}', grid_image, global_step)
        if epoch % args.save_checkpoint_frequency == args.save_checkpoint_frequency - 1:
            save_checkpoint(model, epoch, optimizer, args.checkpoint_save_path, global_step, batch_idx)

if __name__ == '__main__':
    main()


