import os.path
import torch
from torch.utils.data import Dataset
import numpy as np
from common import *

class MovingCoil(Dataset):
    def __init__(self, root='./data_coil', seq_len=seq_len):
        self.img_h = img_h
        self.img_w = img_w
        self.root = os.path.expanduser(root)
        self.filename_list = [s for s in os.listdir(root) if s.endswith('.npy')]
        self.seq_len = seq_len

    def __getitem__(self, index):
        filename = self.filename_list[index]
        d = torch.from_numpy(np.load(os.path.join(self.root, filename)))[:self.seq_len]
        img = d[:, :, :self.img_h, :self.img_w]

        return img.float()

    def __len__(self):
        return len(self.filename_list)