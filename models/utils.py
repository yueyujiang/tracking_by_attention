import torch
import torch.nn.functional as F
from common import *
import os

def draw_box_on_imgs(imgs, z_pres, z_where, apart=False):
    bs = imgs.size(0)
    obj_num = z_pres.size(1)
    z_where = z_where.view(-1, z_where_dim)
    z_pres = z_pres.view(-1, z_pres_dim)
    box = torch.zeros(bs * obj_num, 3, glimpse_size, glimpse_size)
    box[:, :, 0: 3, :] = 1
    box[:, :, -3:, :] = 1
    box[:, :, :, 0: 3] = 1
    box[:, :, :, -3:] = 1
    box = box.to(torch.float32)
    red = torch.tensor([1, 0, 0]).view(1, 3, 1, 1).to(torch.float32)
    green = torch.tensor([0, 1, 0]).view(1, 3, 1, 1).to(torch.float32)
    color = red * (1 - z_pres.view(-1, 1, 1, 1)) + green * z_pres.view(-1, 1, 1, 1)
    box = color * box * (z_pres.view(-1, 1, 1, 1) > 0).to(torch.float32)
    box_slice = affine_transform(box, z_where[:, :2], z_where[:, 2:],
                                 torch.Size((bs * obj_num, 3, img_w, img_h)), to_atten=False)
    if apart:
        ans = (box_slice.view(bs, obj_num, img_channel, img_w, img_h) + imgs).clamp(0, 1)
    else:
        ans = (box_slice.view(bs, obj_num, img_channel, img_w, img_h).sum(1) + imgs).clamp(0, 1)
    return ans

def print_cur_info(global_step, epoch, batch_idx, epoch_samples_num, total_loss, log_like, z_what_kl_loss, z_where_kl_loss,
                z_pres_kl_loss, z_depth_kl_loss, time):
    print('step: {} epoch: {} epoch proportion: {}/{} {:.1f}% total loss: {:<6f} log_like: {:<6f} '
          'z_what_kl_loss: {:<6f} z_where_kl_loss: {:<6f} z_pres_kl_loss: {:<6f} z_depth_kl_loss: {:<6f} time: {:.2f}s'.format(
        global_step,
        epoch,
        batch_idx + 1, epoch_samples_num,
        float(batch_idx + 1) / epoch_samples_num * 100,
        total_loss,
        log_like,
        z_what_kl_loss,
        z_where_kl_loss,
        z_pres_kl_loss,
        z_depth_kl_loss,
        time
    ))

    return

def save_checkpoint(model, epochs, optimizer, checkpoint_PATH, global_step, batch_idx):
    saving_file = {
        'epoch': epochs + 1,
        'state_dict': model.state_dict(),
        'global_step': global_step,
        'batch_idx': batch_idx,
        'optimizer': optimizer.state_dict()
    }
    file_name = "ckpt_epoch_{}.pth.tar".format(epochs + 1)
    file_name = os.path.join(checkpoint_PATH, file_name)
    torch.save(saving_file, file_name)
    return

def load_checkpoint(model, checkpoint_PATH, optimizer):
    model_CKPT = torch.load(checkpoint_PATH)
    model.load_state_dict(model_CKPT['state_dict'])
    print('loading checkpoint {}!'.format(checkpoint_PATH.split('/')[-1]))
    if optimizer:
        optimizer.load_state_dict(model_CKPT['optimizer'])
    global_step = model_CKPT['global_step']
    epoch = model_CKPT['epoch']
    print("finish loading!")
    return epoch, global_step

def affine_transform(img, scale, loc, size, to_atten):
    # img (n, channels, h, w)
    # loc, scale (n, 2)
    device = img.device
    n = img.size(0)
    loc, scale = loc.view(n, 2), scale.view(n, 2)

    theta = torch.zeros(n, 2, 3).to(device)

    if to_atten:
        theta[:, 0, 0] = scale[:, 0]
        theta[:, 1, 1] = scale[:, 1]
        theta[:, 0, 2] = loc[:, 0]
        theta[:, 1, 2] = loc[:, 1]
    else:
        theta[:, 0, 0] = 1 / (scale[:, 0] + 1e-9)
        theta[:, 1, 1] = 1 / (scale[:, 1] + 1e-9)
        theta[:, 0, 2] = - loc[:, 0] / (scale[:, 0] + 1e-9)
        theta[:, 1, 2] = - loc[:, 1] / (scale[:, 1] + 1e-9)

    tmp = F.affine_grid(theta, size)

    output = F.grid_sample(img.to(torch.float32), tmp.to(torch.float32))

    return output

def kl_divergence_bernoulli(z_pres_logits, prior_pres_prob, eps=1e-15):
    z_pres_probs = torch.sigmoid(z_pres_logits).view(-1)
    prior_pres_prob = prior_pres_prob.view(-1)
    kl = z_pres_probs * (torch.log(z_pres_probs + eps) - torch.log(prior_pres_prob + eps)) + \
         (1 - z_pres_probs) * (torch.log(1 - z_pres_probs + eps) - torch.log(1 - prior_pres_prob + eps))

    return kl
