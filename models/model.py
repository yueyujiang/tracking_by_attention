from detection import DetectionCell
from tracking import TrackingCell
from module import *

class MODEL(nn.Module):

    def __init__(self, sigma=.3):
        super(MODEL, self).__init__()

        self.image_enc = ImgEncoder()

        self.z_what_net = GlimpseWhatEnc()
        self.glimpse_dec_net = GlimpseDec()

        self.tracking_cell = nn.DataParallel(
            TrackingCell(z_what_net=self.z_what_net,
                         glimpse_dec_net=self.glimpse_dec_net)
        )

        self.detection_cell = nn.DataParallel(
            DetectionCell(z_what_net=self.z_what_net,
                  glimpse_dec_net=self.glimpse_dec_net)
        )
        self.likelihood_sigma = sigma

        self.register_buffer('z_pres_detect_threshold', torch.tensor(0.7))

        self.prior_init_out = None
        self.prior_init_hid = None

    def initial_seq_hid(self, device):
        return torch.zeros((1, seq_out_dim)).to(device), \
               torch.zeros((1, seq_hid_dim)).to(device)

    def initial_prior_hid(self, device):
        if self.prior_init_out is None or self.prior_init_hid is None:
            self.prior_init_out = nn.Parameter(torch.randn((1, prior_out_dim)),
                                                   requires_grad=True).to(device)
            self.prior_init_hid = nn.Parameter(torch.randn((1, prior_hid_dim)),
                                                   requires_grad=True).to(device)

        return self.prior_init_out, self.prior_init_hid

    def forward(self, seq, tau, eps=1e-15, show=False):

        bs = seq.size(0)
        seq_len = seq.size(1)
        device = seq.device

        kl_z_pres_all = torch.zeros(bs, seq_len)
        kl_z_what_all = torch.zeros(bs, seq_len)
        kl_z_where_all = torch.zeros(bs, seq_len)
        kl_z_depth_all = torch.zeros(bs, seq_len)
        log_like_all = torch.zeros(bs, seq_len)

        tracking_info_list = []
        info_list = []
        x = seq[:, 0]
        img_enc = self.image_enc(x)

        y_slice_cell, alpha_map, z_what, z_where, z_depth, z_pres, \
        kl_z_what_detect, kl_z_where_detect, kl_z_pres_detect, kl_z_depth_detect, log_like_detect, detection_info = \
            self.detection_cell(
                x, img_enc, tau, eps=eps, show=show
            )

        prior_out_init, prior_hid_init = self.initial_prior_hid(device)
        seq_out_init, seq_hid_init = self.initial_seq_hid(device)

        new_prior_out_init = prior_out_init.unsqueeze(0). \
            expand((bs, z_what.size(1), prior_out_dim))
        new_prior_hid_init = prior_hid_init.unsqueeze(0). \
            expand((bs, z_what.size(1), prior_hid_dim))
        new_seq_out_init = seq_out_init.unsqueeze(0). \
            expand((bs, z_what.size(1), seq_out_dim))
        new_seq_hid_init = seq_hid_init.unsqueeze(0). \
            expand((bs, z_what.size(1), seq_hid_dim))

        seq_out_detect = new_seq_out_init
        seq_hid_detect = new_seq_hid_init
        prior_out_detect = new_prior_out_init
        prior_hid_detect = new_prior_hid_init
        z_mask_detect = (z_pres > self.z_pres_detect_threshold).float()

        num_obj_each = torch.sum(z_mask_detect, dim=1)

        max_num_obj = int(torch.max(num_obj_each).item())

        z_what_pre = torch.zeros(bs, max_num_obj, z_what_dim).to(device)
        z_where_pre = torch.zeros(bs, max_num_obj,z_where_dim).to(device)
        z_depth_pre = torch.zeros(bs, max_num_obj, z_depth_dim).to(device)
        z_mask = torch.zeros(bs, max_num_obj, z_pres_dim).to(device)
        seq_out_pre = torch.zeros(bs, max_num_obj, seq_out_dim).to(device)
        seq_hid_pre = torch.zeros(bs, max_num_obj, seq_hid_dim).to(device)
        prior_out_pre = torch.zeros(bs, max_num_obj, prior_out_dim).to(device)
        prior_hid_pre = torch.zeros(bs, max_num_obj, prior_hid_dim).to(device)

        kl_z_pres_all[:, 0] = kl_z_pres_detect
        kl_z_what_all[:, 0] = kl_z_what_detect
        kl_z_where_all[:, 0] = kl_z_where_detect
        kl_z_depth_all[:, 0] = kl_z_depth_detect
        log_like_all[:, 0] = log_like_detect

        if torch.max(num_obj_each) > 0:
            idx0 = z_mask_detect.nonzero()[:, 0]
            idx1 = [torch.arange(int(num_obj_each[i].item())) for i in num_obj_each.nonzero()[:, 0]]
            idx1 = torch.cat(idx1, dim=0)
            nonzero_cell_idx = z_mask_detect.nonzero()[:, 1]
            z_what_pre[idx0, idx1] = z_what[idx0, nonzero_cell_idx]
            z_where_pre[idx0, idx1] = z_where[idx0, nonzero_cell_idx]
            z_depth_pre[idx0, idx1] = z_depth[idx0, nonzero_cell_idx]
            z_mask[idx0, idx1] = z_mask_detect[idx0, nonzero_cell_idx]
            seq_out_pre[idx0, idx1] = seq_out_detect[idx0, nonzero_cell_idx]
            seq_hid_pre[idx0, idx1] = seq_hid_detect[idx0, nonzero_cell_idx]
            prior_out_pre[idx0, idx1] = prior_out_detect[idx0, nonzero_cell_idx]
            prior_hid_pre[idx0, idx1] = prior_hid_detect[idx0, nonzero_cell_idx]

            z_what_pre = z_what_pre.detach()
            z_where_pre = z_where_pre.detach()
            z_depth_pre = z_depth_pre.detach()
            z_mask = z_mask.detach()
            seq_out_pre = seq_out_pre.detach()
            seq_hid_pre = seq_hid_pre.detach()
            prior_out_pre = prior_out_pre.detach()
            prior_hid_pre = prior_hid_pre.detach()

            lengths = torch.sum(z_mask, dim=(1, 2)).view((bs,))

            for i in range(seq_len):
                x = seq[:, i]
                kl_z_what_track = torch.zeros(bs).to(device)
                kl_z_where_track = torch.zeros(bs).to(device)
                kl_z_depth_track = torch.zeros(bs).to(device)
                img_enc = self.image_enc(x)

                y_slice_obj, alpha_map, importance_map, z_what, z_where, \
                z_depth, kl_z_what, kl_z_where, kl_z_depth, seq_out, \
                seq_hid, prior_out, prior_hid, log_like, tracking_info = \
                    self.tracking_cell(
                        x, img_enc, prior_out_pre, prior_hid_pre,
                        seq_out_pre, seq_hid_pre,
                        z_what_pre, z_where_pre, z_depth_pre, lengths, show=show
                    )

                kl_z_pres_all[:, i] = kl_z_pres_detect / seq_len
                kl_z_what_all[:, i] = kl_z_what_detect / seq_len + kl_z_what_track
                kl_z_where_all[:, i] = kl_z_where_detect / seq_len + kl_z_where_track
                kl_z_depth_all[:, i] = kl_z_depth_detect / seq_len + kl_z_depth_track
                log_like_all[:, i] = log_like_detect / seq_len + log_like

                z_what_pre = z_what
                z_where_pre = z_where
                z_depth_pre = z_depth
                seq_out_pre = seq_out
                seq_hid_pre = seq_hid
                prior_out_pre = prior_out
                prior_hid_pre = prior_hid

                if show:
                    info = {
                        'y_slice_obj': y_slice_obj.cpu().detach(),
                        'lengths': lengths
                    }
                else:
                    info = {}

                tracking_info_list.append(tracking_info)
                info_list.append(info)

        return log_like_all.flatten(start_dim=1).mean(dim=1), \
               kl_z_what_all.flatten(start_dim=1).mean(dim=1), \
               kl_z_where_all.flatten(start_dim=1).mean(dim=1), \
               kl_z_depth_all.flatten(start_dim=1).mean(dim=1), \
               kl_z_pres_all.flatten(start_dim=1).mean(dim=1), \
               detection_info, tracking_info_list, info_list