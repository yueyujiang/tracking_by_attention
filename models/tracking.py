import torch.nn.functional as F
from torch.distributions import Normal, kl_divergence
from utils import affine_transform
from module import *

class TrackingCell(nn.Module):
    def __init__(self, z_what_net, glimpse_dec_net, sigma=.3):
        super(TrackingCell, self).__init__()

        self.prior_net = PriorRNN()
        self.seq_rnn = TemporalRnn()

        self.q_net = nn.Linear(seq_out_dim, seq_out_dim)
        self.k_net = nn.Linear(img_encode_dim, seq_out_dim)
        self.v_net = nn.Linear(img_encode_dim, seq_out_dim)

        self.z_where_net = nn.Sequential(
            nn.Linear(z_where_loc_dim + z_where_scale_dim + seq_out_dim, 128),
            nn.CELU(),
            nn.Linear(128, (z_where_scale_dim + z_where_loc_dim) * 2)
        )

        self.z_depth_net = nn.Sequential(
            nn.Linear(z_what_dim + seq_out_dim, 128),
            nn.CELU(),
            nn.Linear(128, z_depth_dim * 2)
        )

        self.z_what_from_seq_net = nn.Sequential(
            nn.Linear(seq_out_dim, z_what_from_seq_hid_dim),
            nn.CELU(),
            nn.Linear(z_what_from_seq_hid_dim, z_what_dim * 2)
        )

        self.what_mean_net = nn.Sequential(
            nn.Linear(z_what_dim * 2, z_what_hid_dim),
            nn.CELU(),
            nn.Linear(z_what_hid_dim, z_what_dim)
        )

        self.what_std_net = nn.Sequential(
            nn.Linear(z_what_dim * 2, z_what_hid_dim),
            nn.CELU(),
            nn.Linear(z_what_hid_dim, z_what_dim)
        )

        self.z_what_net = z_what_net
        self.glimpse_dec_net = glimpse_dec_net
        self.likelihood_sigma = sigma

    def multihead(self, q, k, v, num_head=4):
        # q (bs * max_obj_num, 128)
        # k (bs, 128, 4, 4)
        bs = k.size(0)

        q = self.q_net(q.view(-1, seq_out_dim)).view(bs, -1, seq_out_dim)
        k = self.k_net(k.permute(0, 2, 3, 1).view(bs, -1, img_encode_dim))
        v = self.v_net(v.permute(0, 2, 3, 1).view(bs, -1, img_encode_dim))

        q = q.view(bs, -1, num_head, seq_out_dim // num_head).permute(0, 2, 1, 3).contiguous().view(bs * num_head, -1, seq_out_dim // num_head)
        k = k.view(bs, 4 * 4, num_head, img_encode_dim // num_head).permute(0, 2, 1, 3).contiguous().view(bs * num_head, -1, 4 * 4)
        v = v.view(bs, 4 * 4, num_head, img_encode_dim // num_head).permute(0, 2, 1, 3).contiguous().view(bs * num_head, -1, 4 * 4)

        similarity = torch.bmm(q, k)

        similarity = torch.softmax(similarity, dim=2)

        ans = torch.bmm(similarity, v.permute(0, 2, 1)).view(bs, num_head, -1, seq_out_dim // num_head).\
            permute(0, 2, 1, 3).contiguous().view(bs, -1, seq_out_dim)

        return ans

    def forward(self, x, img_enc, prior_out_pre, prior_hid_pre,
                seq_out_pre, seq_hid_pre,
                z_what_pre, z_where_pre, z_depth_pre, lengths, eps=1e-15, show=False):

        device = x.device
        max_num_obj = prior_hid_pre.size(1)
        bs = x.size(0)
        bns = bs * max_num_obj

        obj_mask = torch.zeros(bs, max_num_obj).to(device)

        for i in range(bs):
            obj_mask[i, :lengths[i].long()] = 1

        prior_out, prior_hid, \
        prior_what_mean, prior_what_std, \
        prior_where_mean, prior_where_std, \
        prior_depth_mean, prior_depth_std = \
            self.prior_net(z_where_pre, z_what_pre, z_depth_pre,
                           prior_out_pre, prior_hid_pre)

        seq_out, seq_hid = \
            self.seq_rnn(z_where_pre, z_what_pre, z_depth_pre,
                              seq_out_pre)

        image_agg_enc = self.multihead(seq_out, img_enc, img_enc)

        z_where_pre = z_where_pre.view(-1,z_where_dim)
        image_agg_enc = image_agg_enc.view(-1, seq_out_dim)

        input = torch.cat((z_where_pre, image_agg_enc), dim=1)

        z_where_bias_mean, z_where_std = self.z_where_net(input).chunk(2, -1)

        z_where_mean = z_where_pre + torch.tanh(z_where_bias_mean)

        z_where_dist = Normal(z_where_mean, z_where_std)

        z_where = z_where_dist.rsample()

        z_where[:, :2] = scale_bias + z_where[:, :2].tanh() * 0.5
        z_where = torch.cat((z_where[:, :2], z_where[:, 2:].clamp(-1.05, 1.05)), dim=1)

        x_att = \
            affine_transform(
                x.unsqueeze(1).expand(-1, max_num_obj, -1, -1, -1).contiguous().view(bns, img_channel, img_h, img_w),
                z_where[:, :2], z_where[:, 2:],
                torch.Size((bns, img_channel, glimpse_size, glimpse_size)), to_atten=True
            )

        z_what_mean_from_enc, z_what_std_from_enc = self.z_what_net(x_att)
        z_what_mean_from_seq, z_what_std_from_seq = \
            self.z_what_from_seq_net(image_agg_enc).chunk(2, -1)

        what_agg_mean = torch.cat((z_what_mean_from_enc, z_what_mean_from_seq), dim=1)
        z_what_mean = self.what_mean_net(what_agg_mean)

        what_agg_std = torch.cat((z_what_std_from_enc, z_what_std_from_seq), dim=1)
        z_what_std = F.softplus(self.what_std_net(what_agg_std))

        z_what_dist = Normal(z_what_mean, z_what_std)
        z_what = z_what_dist.rsample()

        input = torch.cat((z_what, image_agg_enc), dim=1)
        z_depth_mean, z_depth_std = self.z_depth_net(input).chunk(2, -1)

        z_depth_dist = Normal(z_depth_mean, z_depth_std)
        z_depth = z_depth_dist.rsample()

        obj, alpha = self.glimpse_dec_net(z_what)
        alpha_hat = alpha * obj_mask.view(-1, 1, 1, 1)
        y_att = alpha_hat * obj

        y_slice_obj = affine_transform(y_att, z_where[:, :2], z_where[:, 2:],
                                       torch.Size((bns, img_channel, img_h, img_w)), to_atten=False)

        atten_mask_att = alpha_hat * torch.sigmoid(-z_depth).view(-1, 1, 1, 1)

        atten_mask = affine_transform(atten_mask_att, z_where[:, :2], z_where[:, 2:],
                                      torch.Size((bns, 1, img_h, img_w)), to_atten=False)

        alpha_map = affine_transform(alpha_hat, z_where[:, :2], z_where[:, 2:],
                                     torch.Size((bns, 1, img_h, img_w)), to_atten=False)

        prior_what_dist = Normal(prior_what_mean, prior_what_std)
        prior_where_dist = Normal(prior_where_mean, prior_where_std)
        prior_depth_dist = Normal(prior_depth_mean, prior_depth_std)

        kl_z_what = \
            (kl_divergence(z_what_dist, prior_what_dist).sum(1) * \
             obj_mask.view(bns)).view(bs, max_num_obj).sum(1)
        kl_z_where = \
            (kl_divergence(z_where_dist, prior_where_dist).sum(1) * \
             obj_mask.view(bns)).view(bs, max_num_obj).sum(1)
        kl_z_depth = \
            (kl_divergence(z_depth_dist, prior_depth_dist).sum(1) * \
             obj_mask.view(bns)).view(bs, max_num_obj).sum(1)

        z_what = z_what.view(bs, max_num_obj, -1) * obj_mask.view(bs, max_num_obj, 1)
        z_where = z_where.view(bs, max_num_obj, -1) * obj_mask.view(bs, max_num_obj, 1)
        seq_hid = \
            seq_hid.view(bs, max_num_obj, -1) * obj_mask.view(bs, max_num_obj, 1)
        seq_out = \
            seq_out.view(bs, max_num_obj, -1) * obj_mask.view(bs, max_num_obj, 1)
        z_depth = z_depth.view(bs, max_num_obj, -1) * obj_mask.view(bs, max_num_obj, 1)
        y_slice_obj = \
            y_slice_obj.view(bs, max_num_obj, img_channel, img_h, img_w) * obj_mask.view(bs, max_num_obj, 1, 1, 1)
        alpha_map = \
            alpha_map.view(bs, max_num_obj, 1, img_h, img_w) * obj_mask.view(bs, max_num_obj, 1, 1, 1)
        atten_mask = \
            atten_mask.view(bs, max_num_obj, 1, img_h, img_w) * \
            obj_mask.view(bs, max_num_obj, 1, 1, 1)

        prior_out = prior_out.view(bs, max_num_obj, -1)
        prior_hid = prior_hid.view(bs, max_num_obj, -1)

        atten_mask_norm = atten_mask / (atten_mask.sum(dim=1, keepdim=True) + eps)

        y = (y_slice_obj.view(bs, -1, img_channel, img_h, img_w) * atten_mask_norm).sum(dim=1)

        p_x_z = Normal(y.flatten(1), self.likelihood_sigma)
        log_like = p_x_z.log_prob(x.view(-1, img_channel, img_h, img_w).
                                  expand_as(y).flatten(1)).sum(-1)

        if show:
            info = {
                'z_what': z_what,
                'z_where': z_where,
                'obj_mask': obj_mask,
                'z_depth': z_depth,
                'alpha_map': alpha_map.view(bs, max_num_obj, 1, img_h, img_w),
                'atten_mask_norm': atten_mask_norm.view(bs, max_num_obj, 1, img_h, img_w),
                'obj': obj.view(bs, max_num_obj, img_channel, glimpse_size, glimpse_size),
                'y_slice_obj': y_slice_obj.view(bs, max_num_obj, img_channel, img_h, img_w),
                'y': y
            }
        else:
            info = {}

        return y_slice_obj, alpha_map, atten_mask, z_what, z_where, \
               z_depth, kl_z_what, kl_z_where, kl_z_depth, seq_out, \
               seq_hid, prior_out, prior_hid, log_like, info


