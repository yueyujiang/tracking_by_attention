import torch
import torch.nn as nn
from common import *

class ImgEncoder(nn.Module):

    def __init__(self):
        super(ImgEncoder, self).__init__()

        self.enc = nn.Sequential(
            nn.Conv2d(img_channel, 16, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(4, 16),
            nn.Conv2d(16, 32, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(8, 32),
            nn.Conv2d(32, 64, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(8, 64),
            nn.Conv2d(64, 128, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(16, 128),
            nn.Conv2d(128, 128, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(16, 128),
            nn.Conv2d(128, img_encode_dim, 1),
            nn.CELU(),
            nn.GroupNorm(16, img_encode_dim)
        )

    def forward(self, x):
        img_enc = self.enc(x)

        return img_enc

class GlimpseWhatEnc(nn.Module):

    def __init__(self):
        super(GlimpseWhatEnc, self).__init__()

        self.enc_cnn = nn.Sequential(
            nn.Conv2d(img_channel, 16, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(4, 16),
            nn.Conv2d(16, 32, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(8, 32),
            nn.Conv2d(32, 64, 4, 2, 1),
            nn.CELU(),
            nn.GroupNorm(8, 64),
            nn.Conv2d(64, 64, 3, 1, 1),
            nn.CELU(),
            nn.GroupNorm(8, 64),
            nn.Conv2d(64, 128, 4),
            nn.CELU(),
            nn.GroupNorm(16, 128),
        )

        self.enc_what = nn.Linear(128, z_what_dim * 2)

    def forward(self, x):
        x = self.enc_cnn(x)

        z_what_mean, z_what_std = self.enc_what(x.flatten(start_dim=1)).chunk(2, -1)

        return z_what_mean, z_what_std


class GlimpseDec(nn.Module):

    def __init__(self):
        super(GlimpseDec, self).__init__()

        self.dec = nn.Sequential(
            nn.Conv2d(z_what_dim, 256, 1),
            nn.CELU(),
            nn.GroupNorm(16, 256),

            nn.Conv2d(256, 128 * 4 * 4, 1),
            nn.PixelShuffle(4),
            nn.CELU(),
            nn.GroupNorm(16, 128),
            nn.Conv2d(128, 128, 3, 1, 1),
            nn.CELU(),
            nn.GroupNorm(16, 128),

            nn.Conv2d(128, 64 * 4 * 4, 1),
            nn.PixelShuffle(4),
            nn.CELU(),
            nn.GroupNorm(8, 64),
            nn.Conv2d(64, 64, 3, 1, 1),
            nn.CELU(),
            nn.GroupNorm(8, 64),

            nn.Conv2d(64, 16 * 2 * 2, 1),
            nn.PixelShuffle(2),
            nn.CELU(),
            nn.GroupNorm(4, 16),
            nn.Conv2d(16, 16, 3, 1, 1),
            nn.CELU(),
            nn.GroupNorm(4, 16),

        )

        self.dec_o = nn.Conv2d(16, img_channel, 3, 1, 1)

        self.dec_alpha = nn.Conv2d(16, 1, 3, 1, 1)

    def forward(self, x):
        x = self.dec(x.view(x.size(0), -1, 1, 1))

        o = torch.sigmoid(self.dec_o(x))
        alpha = torch.sigmoid(self.dec_alpha(x))

        return o, alpha

class TemporalRnn(nn.Module):
    def __init__(self):
        super(TemporalRnn, self).__init__()

        input_dim = z_dim - z_pres_dim
        self.seq = nn.GRUCell(input_dim, 128)

    def forward(self, z_where_pre, z_what_pre, z_depth_pre, seq_out_pre):

        z_where_pre = z_where_pre.view(-1,z_where_dim)
        z_what_pre = z_what_pre.view(-1, z_what_dim)
        z_depth_pre = z_depth_pre.view(-1, z_depth_dim)
        seq_out_pre = seq_out_pre.view(-1, seq_out_dim)

        input = torch.cat((z_where_pre, z_what_pre, z_depth_pre), dim=1)
        h = self.seq(input, seq_out_pre)

        seq_out = h
        seq_hid = h

        return seq_out, seq_hid

class PriorRNN(nn.Module):
    def __init__(self):
        super(PriorRNN, self).__init__()

        self.prior = \
            nn.LSTMCell(z_dim - z_pres_dim, 128)

        self.z_what_net = nn.Linear(prior_out_dim, z_what_dim * 2)

        self.z_where_net = nn.Linear(prior_out_dim, (z_where_loc_dim + z_where_scale_dim) * 2)

        self.z_depth_net = nn.Linear(prior_out_dim, z_depth_dim * 2)

    def forward(self, z_where_pre, z_what_pre, z_depth_pre, prior_out_pre, prior_hid_pre):

        z_where_pre = z_where_pre.view(-1,z_where_dim)
        z_what_pre = z_what_pre.view(-1, z_what_dim)
        z_depth_pre = z_depth_pre.view(-1, z_depth_dim)
        input = \
            torch.cat((z_where_pre, z_what_pre, z_depth_pre), dim=1)

        prior_hid_pre = prior_hid_pre.view(-1, prior_hid_dim)
        prior_out_pre = prior_out_pre.view(-1, prior_out_dim)

        h, c = self.prior(input, (prior_out_pre, prior_hid_pre))

        prior_hid, prior_out = c, h

        prior_what_mean, prior_what_std = self.z_what_net(prior_out).chunk(2, -1)
        prior_where_mean, prior_where_std = self.z_where_net(prior_out).chunk(2, -1)
        prior_depth_mean, prior_depth_std = self.z_depth_net(prior_out).chunk(2, -1)

        return prior_out, prior_hid, \
               prior_what_mean, prior_what_std, \
               prior_where_mean, prior_where_std, \
               prior_depth_mean, prior_depth_std




