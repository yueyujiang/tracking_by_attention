img_channel = 3
z_what_dim = 64
z_where_scale_dim = 2
z_where_loc_dim = 2
z_where_dim = z_where_scale_dim + z_where_loc_dim
z_pres_dim = 1
glimpse_size = 32
glimpse_enc_dim = 32
img_h = 128
img_w = img_h
img_encode_dim = 128
img_encode_detect_dim = img_encode_dim * 4
z_depth_dim = 1

scale_bias = 0.2
seq_hid_dim = 128
seq_out_dim = seq_hid_dim
z_where_transit_bias_net_hid_dim = 128

z_where_hid_dim = 128
z_what_hid_dim = 256
z_pres_hid_dim = 128
z_what_from_seq_hid_dim = 128

prior_hid_dim = 128
prior_out_dim = prior_hid_dim

detect_rnn_hid_dim = 128
detect_rnn_out_dim = detect_rnn_hid_dim

seq_len = 10
num_head = 4
dim_value = img_encode_dim
detect_input_dim = 3 * (z_where_loc_dim + z_where_scale_dim) + 3 * z_what_dim + 3 * z_pres_dim + 3 * z_depth_dim + img_encode_dim
detect_z_pre_agg_dim = 3 * (z_where_loc_dim + z_where_scale_dim) + 3 * z_what_dim + 3 * z_pres_dim + 3 * z_depth_dim
detect_z_pre_dim = 64

z_dim = z_where_scale_dim + z_where_loc_dim + z_what_dim + z_pres_dim + z_depth_dim
