import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal, kl_divergence, RelaxedBernoulli
from utils import affine_transform, kl_divergence_bernoulli
from common import *

class DetectionCell(nn.Module):

    def __init__(self, z_what_net, glimpse_dec_net, sigma=0.3):
        super(DetectionCell, self).__init__()

        self.z_where_net = nn.Sequential(
            nn.Linear(detect_input_dim, z_where_transit_bias_net_hid_dim),
            nn.CELU(),
            nn.Linear(z_where_transit_bias_net_hid_dim, (z_where_scale_dim + z_where_loc_dim) * 2)
        )

        self.z_pres_net = nn.Sequential(
            nn.Linear(detect_input_dim, z_where_transit_bias_net_hid_dim),
            nn.CELU(),
            nn.Linear(z_where_transit_bias_net_hid_dim, z_pres_dim)
        )

        self.z_depth_net = nn.Sequential(
            nn.Linear(detect_input_dim, z_where_transit_bias_net_hid_dim),
            nn.CELU(),
            nn.Linear(z_where_transit_bias_net_hid_dim, z_depth_dim * 2)
        )


        self.likelihood_sigma = sigma


        self.z_what_net = z_what_net
        self.glimpse_dec = glimpse_dec_net
        self.likelihood_sigma = sigma

        self.register_buffer('prior_what_mean', torch.zeros(1))
        self.register_buffer('prior_what_std', torch.ones(1))
        self.register_buffer('prior_bg_mean', torch.zeros(1))
        self.register_buffer('prior_bg_std', torch.ones(1))
        self.register_buffer('prior_depth_mean', torch.zeros(1))
        self.register_buffer('prior_depth_std', torch.ones(1))
        self.register_buffer('prior_where_mean',
                             torch.tensor([0., 0., 0., 0.]).view((z_where_scale_dim + z_where_loc_dim), 1, 1))
        self.register_buffer('prior_where_std',
                             torch.tensor([1., 1., 1., 1.]).view((z_where_scale_dim + z_where_loc_dim), 1, 1))
        self.register_buffer('prior_z_pres_prob', torch.tensor(1e-2))

    @property
    def p_z_what(self):
        return Normal(self.prior_what_mean, self.prior_what_std)

    @property
    def p_z_depth(self):
        return Normal(self.prior_depth_mean, self.prior_depth_std)

    @property
    def p_z_where(self):
        return Normal(self.prior_where_mean, self.prior_where_std)
    
    def image_encode(self, input, tau):

        z_pres_logits = self.z_pres_net(input)

        z_pres_dist = RelaxedBernoulli(logits=z_pres_logits, temperature=tau)

        z_pres = torch.sigmoid(z_pres_dist.rsample())

        # (bs, dim, 4, 4)
        z_depth_mean, z_depth_std = self.z_depth_net(input).chunk(2, 1)
        z_depth_std = F.softplus(z_depth_std)
        q_z_depth = Normal(z_depth_mean, z_depth_std)

        z_depth = q_z_depth.rsample()

        # (bs, 4 + 4, 4, 4)
        z_where_mean, z_where_std = self.z_where_net(input).chunk(2, 1)
        z_where_std = F.softplus(z_where_std)

        z_where_dist = Normal(z_where_mean, z_where_std)

        z_where = z_where_dist.rsample()

        z_where[:, :2] = scale_bias + z_where[:, :2].tanh() * 0.5
        z_where[:, 2:] = 0.5 * z_where[:, 2:].tanh()

        return z_where, z_pres, z_depth, z_where_mean, z_where_std, \
               z_depth_mean, z_depth_std, z_pres_logits

    def forward(self, x, img_enc, tau, eps=1e-15, show=False):
        device = x.device
        bs = x.size(0)

        z_where_map = torch.zeros(bs,z_where_dim, 4, 4).to(device)
        z_what_map = torch.zeros(bs, z_what_dim, 4, 4).to(device)
        z_pres_map = torch.zeros(bs, z_pres_dim, 4, 4).to(device)
        z_depth_map = torch.zeros(bs, z_depth_dim, 4, 4).to(device)

        z_pres_logits_map = torch.zeros(bs, z_pres_dim, 4, 4).to(device)
        z_where_std_map = torch.zeros(bs,z_where_dim, 4, 4).to(device)
        z_what_std_map = torch.zeros(bs, z_what_dim, 4, 4).to(device)
        z_depth_std_map = torch.zeros(bs, z_depth_dim, 4, 4).to(device)

        z_where_mean_map = torch.zeros(bs,z_where_dim, 4, 4).to(device)
        z_what_mean_map = torch.zeros(bs, z_what_dim, 4, 4).to(device)
        z_depth_mean_map = torch.zeros(bs, z_depth_dim, 4, 4).to(device)

        x_coor = torch.linspace(-0.75, 0.75, 4).to(device)
        y_coor = torch.linspace(-0.75, 0.75, 4).to(device)
        y_grid, x_grid = torch.meshgrid(x_coor, y_coor)
        coor_grid = torch.cat((x_grid.view(1, 4, 4), y_grid.view(1, 4, 4)), dim=0)
        z_where_map[:, z_where_loc_dim:] = coor_grid

        y_out = []
        atten_mask_full_res_out = []
        alpha_map_out = []
        x_att_out = []
        y_att_out = []
        obj_out = []
        alpha_hat_out = []
        alpha_out = []
        for i in range(4):
            for j in range(4):
                z_where_left_pre, z_what_left_pre, z_pres_left_pre, z_depth_left_pre = \
                    (z_where_map[:, :, i, j - 1], z_what_map[:, :, i, j - 1], \
                    z_pres_map[:, :, i, j - 1], z_depth_map[:, :, i, j - 1]) \
                        if j > 0 else \
                        (torch.zeros(bs,z_where_dim).to(device), \
                        torch.zeros(bs, z_what_dim).to(device), torch.zeros(bs, z_pres_dim).to(device), \
                        torch.zeros(bs, z_depth_dim).to(device))
                z_where_top_pre, z_what_top_pre, z_pres_top_pre, z_depth_top_pre = \
                    (z_where_map[:, :, i - 1, j], z_what_map[:, :, i - 1, j], \
                    z_pres_map[:, :, i - 1, j], z_depth_map[:, :, i - 1, j]) \
                        if i > 0 else \
                        (torch.zeros(bs,z_where_dim).to(device), \
                    torch.zeros(bs, z_what_dim).to(device), torch.zeros(bs, z_pres_dim).to(device), \
                    torch.zeros(bs, z_depth_dim).to(device))
                z_where_left_top_pre, z_what_left_top_pre, z_pres_left_top_pre, z_depth_left_top_pre = \
                    (z_where_map[:, :, i - 1, j - 1], z_what_map[:, :, i - 1, j - 1], \
                    z_pres_map[:, :, i - 1, j - 1], z_depth_map[:, :, i - 1, j - 1]) \
                        if j > 0 and i > 0 else \
                        (torch.zeros(bs,z_where_dim).to(device), \
                    torch.zeros(bs, z_what_dim).to(device), torch.zeros(bs, z_pres_dim).to(device), \
                    torch.zeros(bs, z_depth_dim).to(device))
                z_left_pre = torch.cat(
                    (z_where_left_pre, z_what_left_pre, z_pres_left_pre, z_depth_left_pre), dim=1)
                z_top_pre = torch.cat(
                    (z_where_top_pre, z_what_top_pre, z_pres_top_pre, z_depth_top_pre), dim=1)
                z_left_top_pre = torch.cat(
                    (z_where_left_top_pre, z_what_left_top_pre, z_pres_left_top_pre, z_depth_left_top_pre), dim=1)

                input = torch.cat((img_enc[:, :, i, j], z_left_pre, z_top_pre, z_left_top_pre), dim=1)

                z_where, z_pres, z_depth, z_where_mean, z_where_std, \
                    z_depth_mean, z_depth_std, z_pres_logits = self.image_encode(input, tau)

                z_where_map[:, :, i, j] += z_where
                z_where = z_where_map[:, :, i, j]
                z_pres_map[:, :, i, j] = z_pres
                z_depth_map[:, :, i, j] = z_depth
                z_where_mean_map[:, :, i, j] = z_where_mean
                z_where_std_map[:, :, i, j] = z_where_std
                z_depth_mean_map[:, :, i, j] = z_depth_mean
                z_depth_std_map[:, :, i, j] = z_depth_std
                z_pres_logits_map[:, :, i, j] = z_pres_logits

                x_att = affine_transform(x, z_where[:, :2], z_where[:, 2:],
                                         torch.Size((bs, img_channel, glimpse_size, glimpse_size)), to_atten=True)

                z_what_mean, z_what_std = self.z_what_net(x_att)
                z_what_std = F.softplus(z_what_std)
                q_z_what = Normal(z_what_mean, z_what_std)

                z_what = q_z_what.rsample()

                z_what_map[:, :, i, j] = z_what
                z_what_mean_map[:, :, i, j] = z_what_mean
                z_what_std_map[:, :, i, j] = z_what_std

                obj, alpha = self.glimpse_dec(z_what)

                alpha_hat = alpha * z_pres.view(-1, 1, 1, 1)
                y_att = alpha_hat * obj

                y_slice_cell = affine_transform(y_att, z_where[:, :2], z_where[:, 2:],
                                                torch.Size((bs, img_channel, img_h, img_w)),
                                                to_atten=False)

                atten_mask = 5 * alpha_hat * torch.sigmoid(-z_depth).view(-1, 1, 1, 1)

                atten_mask_full_res = affine_transform(atten_mask, z_where[:, :2], z_where[:, 2:],
                                                       torch.Size((bs, 1, img_h, img_w)),
                                                       to_atten=False)

                alpha_map = affine_transform(alpha_hat, z_where[:, :2], z_where[:, 2:],
                                              torch.Size((bs, 1, img_h, img_w)),
                                              to_atten=False)
                y_out.append(y_slice_cell.view(-1, 1, img_channel, img_h, img_w))
                atten_mask_full_res_out.append(atten_mask_full_res.view(-1, 1, 1, img_h, img_w))
                alpha_map_out.append(alpha_map.view(-1, 1, 1, img_h, img_w))
                x_att_out.append(x_att.view(-1, 1, img_channel, glimpse_size, glimpse_size))
                y_att_out.append(y_att.view(-1, 1, img_channel, glimpse_size, glimpse_size))
                obj_out.append(obj.view(-1, 1, 1, glimpse_size, glimpse_size))
                alpha_hat_out.append(alpha_hat.view(-1, 1, 1, glimpse_size, glimpse_size))
                alpha_out.append(alpha.view(-1, 1, 1, glimpse_size, glimpse_size))


        z_where = z_where_map.permute(0, 2, 3, 1)
        z_what = z_what_map.permute(0, 2, 3, 1)
        z_pres = z_pres_map.permute(0, 2, 3, 1)
        z_depth = z_depth_map.permute(0, 2, 3, 1)

        z_pres_logits = z_pres_logits_map.permute(0, 2, 3, 1)

        z_where_std = z_where_std_map.permute(0, 2, 3, 1)
        z_what_std = z_what_std_map.permute(0, 2, 3, 1)
        z_depth_std = z_depth_std_map.permute(0, 2, 3, 1)

        z_where_mean = z_where_mean_map.permute(0, 2, 3, 1)
        z_what_mean = z_what_mean_map.permute(0, 2, 3, 1)
        z_depth_mean = z_depth_mean_map.permute(0, 2, 3, 1)

        y_slice_cell = torch.cat(y_out, dim=1)
        atten_mask_full_res = torch.cat(atten_mask_full_res_out, dim=1)
        alpha_map = torch.cat(alpha_map_out, dim=1)
        x_att = torch.cat(x_att_out, dim=1)
        y_att = torch.cat(y_att_out, dim=1)
        obj = torch.cat(obj_out, dim=1)
        alpha_hat = torch.cat(alpha_hat_out, dim=1)
        alpha = torch.cat(alpha_out, dim=1)

        z_pres_origin = z_pres

        z_pres = z_pres.view(bs, -1)

        q_z_what = Normal(z_what_mean, z_what_std)
        z_where_dist = Normal(z_where_mean, z_where_std)
        q_z_depth = Normal(z_depth_mean, z_depth_std)

        kl_z_what = kl_divergence(q_z_what, self.p_z_what) * z_pres_origin.view(-1, 4, 4, z_pres_dim)
        # (bs, 4 * 4, z_what_dim)
        kl_z_what = kl_z_what.view(-1, 4 * 4, z_what_dim)
        # (4 * 4 * bs, z_depth_dim)
        kl_z_depth = kl_divergence(q_z_depth, self.p_z_depth) * z_pres_origin.view(-1, 4, 4, z_pres_dim)
        # (bs, 4 * 4, z_depth_dim)
        kl_z_depth = kl_z_depth.view(-1, 4 * 4, z_depth_dim)
        # (bs, dim, 4, 4)
        kl_z_where = kl_divergence(z_where_dist, self.p_z_where) * z_pres_origin.view(-1, 4, 4, z_pres_dim)
        kl_z_where = kl_z_where.view(-1, 4 * 4,z_where_dim)

        kl_z_pres = kl_divergence_bernoulli(z_pres_logits.view(-1, z_pres_dim), self.prior_z_pres_prob)
        kl_z_pres = kl_z_pres.view(-1, 4 * 4, z_pres_dim)

        y_slice_cell = y_slice_cell.view(bs, 4 * 4, img_channel, img_h, img_w)
        alpha_map = alpha_map.view(bs, 4 * 4, 1, img_h, img_w)
        atten_mask_full_res = atten_mask_full_res.view(bs, 4 * 4, 1, img_h, img_w)
        z_what = z_what.view(bs, 4 * 4, -1)
        z_where = z_where.view(bs, 4 * 4, -1)
        z_depth = z_depth.view(bs, 4 * 4, -1)
        z_pres = z_pres.view(bs, 4 * 4, -1)
        kl_z_what = kl_z_what.flatten(start_dim=1).sum(dim=1)
        kl_z_where = kl_z_where.flatten(start_dim=1).sum(dim=1)
        kl_z_pres = kl_z_pres.flatten(start_dim=1).sum(dim=1)
        kl_z_depth = kl_z_depth.flatten(start_dim=1).sum(dim=1)

        atten_mask_norm = \
            atten_mask_full_res / (atten_mask_full_res.sum(dim=1, keepdim=True) + eps)

        y = (y_slice_cell.view(bs, -1, img_channel, img_h, img_w) * atten_mask_norm).sum(dim=1)
        
        p_x_z = Normal(y.flatten(1), self.likelihood_sigma)
        log_like_detect = p_x_z.log_prob(x.view(-1, img_channel, img_h, img_w).
                                         expand_as(y).flatten(1)).sum(-1)


        if show:
            info = {
                'z_what': z_what,
                'z_where': z_where,
                'x_att': x_att,
                'y_att': y_att,
                'obj': obj,
                'alpha_hat': alpha_hat,
                'alpha': alpha,
                'y_slice_cell': y_slice_cell,
                'z_depth': z_depth,
                'z_pres': z_pres,
                'y': y,
                'atten_mask': atten_mask_full_res
            }
        else:
            info = {}

        assert y_slice_cell.size() == torch.Size([bs, 4 * 4, img_channel, img_h, img_w])
        assert alpha_map.size() == torch.Size([bs, 4 * 4, 1, img_h, img_w])
        assert atten_mask_full_res.size() == torch.Size([bs, 4 * 4, 1, img_h, img_w])
        assert z_what.size() == torch.Size([bs, 4 * 4, z_what_dim])
        assert z_where.size() == torch.Size([bs, 4 * 4, 4])
        assert z_depth.size() == torch.Size([bs, 4 * 4, 1])
        assert z_pres.size() == torch.Size([bs, 4 * 4, 1])


        return y_slice_cell, alpha_map, \
               z_what, z_where, z_depth, z_pres, kl_z_what, kl_z_where, \
               kl_z_pres, kl_z_depth, log_like_detect, info