## Attention-based Unsupervised Object Detection and Tracking 
This is an individual project for object detection and tracking, and it is purely unsupervised. Model consists of two part, object 
detection part and object tracking. In object detection network, I apply spatially invariant object detection network [1] for object detection. Follows the detection network, 
attention-based (transformer) trackers are used to track the moving object. The overall model are developed under the VAE framework.

[1] [spatially invariant object detection network](http://116.199.9.133:81/2Q2W61FA59E95CA40145BE381291945126AA68F4181E_unknown_B0EF84FF0D9F877466A1ACBFED538AD6A885580D_5/e2crawfo.github.io/pdfs/spair_nips_workshop.pdf)

### Result
Following are some samples from the testset. Notice that each testing visualization sample showed below has a much longer sequence length than each traning sequence has. This fully illustrate the generalization of the method.

#### Detection
* First line are the samples from the testset
* Second line are the reconstruction of images
* Third line are the detection results  
 
![Semantic description of image](./result/detection.png)

#### Tracking 
* First line are the samples from the testset
* Second line are the reconstruction of images
* Third line are the tracking results  

![Semantic description of image](./result/tracking.gif)

For more results examples, please refer to https://vimeo.com/360290966

### Training
> python train.py ----training-data-dir "path/for/the/training_data"